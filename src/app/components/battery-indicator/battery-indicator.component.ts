import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-battery-indicator',
  templateUrl: './battery-indicator.component.html',
  styleUrls: ['./battery-indicator.component.scss'],
})
export class BatteryIndicatorComponent implements OnInit {
  /* Golden or simple blue */
  @Input() category: string;
  @Input() value: bigint;

  constructor() { }

  ngOnInit() {}

}
