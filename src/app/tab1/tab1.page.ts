import { Component } from '@angular/core';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  selectedQrCodes: Array<string>;
  manualEnteredQrCode = '';

  constructor(private barecodeScanner: BarcodeScanner) {

    /* activate the scanner */
    // this.openQrCodeScanner();

  }

  processManualEnteredQrCode() {
      /* Check if the entered qrCode is valid */
      if ( this.manualEnteredQrCode.length >= 13 ) {
          this.selectedQrCodes.push(this.manualEnteredQrCode);
          this.manualEnteredQrCode = '';
          this.notifyUser('QrCode ajouté');
      } else {
          this.notifyUser('Saisissez un QrCode valide');
      }

  }

  openQrCodeScanner() {

    this.barecodeScanner.scan(
        {
          preferFrontCamera : false, // iOS and Android
          showFlipCameraButton : true, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: true, // Android, launch with the torch switched on (if available)
          prompt : 'Visez le premier code à scanner', // Android
          resultDisplayDuration: 1500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : 'QR_CODE,PDF_417', // default: all but PDF_417 and RSS_EXPANDED
          orientation : 'portrait', // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : false, // iOS
          disableSuccessBeep: false // iOS and Android
        }
    ).then(
        barcodeData  => {
          console.log(barcodeData);
          this.selectedQrCodes.push(barcodeData.text);
        }
    );
  }

  notifyUser(msg: string) {}

}
